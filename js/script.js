$('#parallax-1').parallax();
/* Loader : Show loading animation before page load
======================================================*/
$(window).load(function(){
  $('#dvLoading').fadeOut();
});

$(document).ready(function(){
	  function setHeight() {
	    windowHeight = $(window).height();
	    $('.wHeight').css('height', windowHeight).css("width", "100%");
	  };

	  setHeight();
	  
	  $(window).resize(function() {
	    setHeight();
	  });

	 if ($(window).width() > 1024) {
		 $(window).scroll(function() {
	       $(".front").css("margin-top",-($(window).scrollTop()/3));
	       $(".obj-left").css("top",-($(window).scrollTop()/5));
	       $(".single.alfamart #prize-image, .single.alfamidi #prize-image, .single.indomaret #prize-image").css("bottom", ($(window).scrollTop()/3));
	       $(".single #header").css("margin-top", - ($(window).scrollTop()/4));
	    });
	};
	//logo folding effect
	 setTimeout(function() {
      	$("#logo").addClass("active")
	}, 500);

	setTimeout(function() {
      	$(".page").css("opacity", "1")
	}, 100);

	//sun delay effect
	 setTimeout(function() {
      	$("#sun img").addClass("active")
	}, 1000);

	 $(".btn-primary.to-page2").click(function() {
	    $('html, body').animate({
	        scrollTop: $("#page-2").offset().top
	    }, 2000);
	    return false;
	});

	$(".btn-primary.yellow").click(function() {
	    $('html, body, #page-3').animate({
	        scrollTop: $("#page-4").offset().top
	    }, 4000);
	    return false;
	});


	
	$(".single article ul.list-with-images li .details").css("width", $(".single article ul.list-with-images").width() - $(".single article ul.list-with-images li img").width() - 30);

	$(window).resize(function() {
	    $(".single article ul.list-with-images li .details").css("width", $(".single article ul.list-with-images").width() - $(".single article ul.list-with-images li img").width() - 30);
	  });
});
