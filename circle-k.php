<?php
 $title = "Circle K | AQUA -  Temukan Indonesiamu";
 $bodyClass = "single circle-k";
 include('header.php') ?>
    <header id="header" class="">
        <div class="logo w-1000"><img src="images/single/ck-logo.png" alt="Temukan Indonesiamu di AlfaMidi/AlfaExpress/Lawson"></div>
        <div class="mobile-content">
            <img src="images/single/ck-prize-mobile.png" class="prize">
        </div>
        <div id="prize-image">
            <img src="images/single/ck-prize.png">
        </div>
    </header>     

    <div id="post" class="row clearfix">
        <article class="half">
            <h3>Caranya Mudah</h3>
            <ul class="list-with-images">
                <li>
                    <img src="images/single/alfamart-step1.png">
                    <div class="details">
                        <p>Beli <strong>3 botol AQUA</strong> kemasan 600 ml di Circle K yang di wilayah DI Yogyakarta untuk ikut bermain.</p>
                    </div>
                </li>
                <li>
                    <img src="images/single/ck-step2.png">
                    <div class="details">
                        <p>Ajak teman-teman kamu berfoto di booth Temukan Indonesiamu dengan membawa 3 botol  AQUA 600 ml yang kamu beli.</p>
                    </div>
                </li>
                 <li>
                    <img src="images/single/ck-step3.png">
                    <div class="details">
                        <p>Upload foto kamu ke Twitter, tuliskan kode struk dan follow + mention @SehatAQUA &amp; @CircleKIndo dengan #TemukanIndonesiamu #TICK</p>
                    </div>
                </li>
            </ul>

            <h3>Ketentuan</h3>
            <ol>
                <li>Promo hanya berlaku di Circle K yang berpartisipasi di wilayah Yogyakarta</li>
                <li>Pemenang akan diumumkan 3 minggu setelah periode promo berakhir</li>
            </ol>
        </article>
        <aside id="sidebar">
            <div class="box">
                <h3>Periode Promo</h3>
                <span>7 Desember 2014 - 31 Desember 2014</span>
            </div>
            <div class="share-button">
                <h3>Share This Page</h3>
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $actual_link; ?>" target="_blank"><img src="images/single/fb.png"></a>
                <a href="http://twitter.com/intent/tweet?text=<?php echo $title; ?> <?php echo $actual_link; ?>"><img src="images/single/twitter.png"></a>
               <!--  <a href=""><img src="images/single/gplus.png"></a> -->
            </div>
        </aside>
    </div>

<?php include('footer.php') ?>