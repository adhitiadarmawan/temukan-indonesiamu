<?php
 $title = "AQUA -  Temukan Indonesiamu";
 $bodyClass = "home";
 include('header.php') ?>
        <div id="page-1"  class="page">
                <div id="logo-text" class="layer w-1000">
                    <div>
                        <img src="images/logo-text.png">
                        <span>Kenali Indonesiamu melalui kebersamaan dalam permainan tradisional!</span>
                    </div>
                </div>
                <div class="desktop-content">
                <div id="land"></div>
                <div id="sun" class="w-1000 layer"><img src="images/matahari.png" alt="Matahati"></div>
                <div class="layer w-1000 bottom botol"><img src="images/bottle.png" class="bottle" alt="Botol AQUA"></div>
                <div class="layer w-1000 child"><img src="images/bocah.png" class="bocah" alt="Art"></div>
                <div class="layer bottom obj-left" style="z-index:10;"><img src="images/gedung-kiri.png" class="gedung-kiri" alt="gedung"></div>
                <ul id="parallax-1" class="parallax">
                     <li class="layer cloud-left" data-depth="0.30"><img src="images/awan-kiri.png" class="awan-kiri" alt="Awan"></li>                 
                     <li class="layer cloud-right" data-depth="0.40"><img src="images/awan-kanan.png" class="awan-kanan"alt="Awan"></li>
                     <li class="layer" data-depth="0.20"><img src="images/awan-atas.png" class="awan-atas" alt="Awan"></li>         
                     <li class="layer  w-1000" data-depth="0.10"><img src="images/edisi-text.png" class="edisi" alt="Edisi"></li>    
                </ul>

            </div><!--END DESKTOP CONTENT-->
                <div id="button-container" class="wHeight absolute">
                    <div class="floating-button">
                        <a href="#page-2" class="btn-primary to-page2">ayo ikutan, banyak hadiah menarik di dalamnya! <i class="fa fa-arrow-circle-down"></i></a>
                    </div>
                </div>
        </div>
        <div id="page-2" class="page">
            <div id="alfamart">
                <div class="desktop-content">
                    <ul class="list">
                        <li>
                            <div class="box">
                            <p>Menangkan 20 paket liburan ke Raja Ampat untuk kamu dan 2 orang teman atau keluargamu!</p>
                            <a href="alfamart" class="btn-primary">Selengkapnya</a>
                            </div>
                        </li>
                        <li>
                            <div class="periode">
                            <p>
                                <strong>Periode Promo</strong><br>
                                1 Desember 2014 - 15 Januari 2015
                            </p>
                            </div>
                        </li>
                        <li><img src="images/alfamart-prize.png" alt="Alfamart Prize"></li>
                    </ul>
                </div>

                <div class="mobile-content">
                    <div class="row text-center">
                        <img src="images/mobile/alfamart-prize.png" alt="Alfamart Prize">
                        <p class="description">Menangkan 20 paket liburan ke Raja Ampat untuk kamu dan 2 orang teman atau keluargamu!</p>
                        <a href="alfamart" class="btn-primary">Selengkapnya</a>
                        <div class="periode">
                            <p>
                                <strong>Periode Promo</strong><br>
                                1 Desember 2014 - 15 Januari 2015
                            </p>
                         </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="page-3" class="page front">
            <div id="alfamidi">
                <div class="desktop-content">
                    <div class="row clearfix">
                        <div class="prize left"><img src="images/alfamidi-prize.png" alt="Alfamidi Prize"></div>
                        <div class="intro left">
                            <p>Menangkan 20 paket liburan ke Raja Ampat untuk kamu dan 2 orang teman atau keluargamu !</p>
                            <a href="alfamidi" class="btn-primary">Selengkapnya</a>
                        </div>
                        <div class="headline right"><img src="images/alfamidi-headline.png"></div>
                    </div>
                </div>
                <div class="mobile-content">
                    <div class="row text-center">
                        <img src="images/mobile/alfamidi-prize.png" alt="Alfamidi Prize">
                        <p class="description">Menangkan 20 paket liburan ke Raja Ampat untuk kamu dan 2 orang teman atau keluargamu!</p>
                        <a href="alfamidi" class="btn-primary">Selengkapnya</a>
                        <div class="periode">
                            <p>
                                <strong>Periode Promo</strong><br>
                                1 Desember 2014 - 15 Januari 2015
                            </p>
                         </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a href="#page-4" class="btn-primary yellow to-page-4">Masih Ada lagi <i class="fa fa-arrow-circle-o-down"></i></a>
            </div>
        </div>
        <div id="page-4" class="page front">
            <div id="indomaret" class="row clearfix">
                <div class="desktop-content">
                    <div class="headline left">
                        <img src="images/indomaret-headline.png">
                        <p>Menangkan 20 paket liburan ke Raja Ampat untuk kamu dan 2 orang teman atau keluargamu !</p>
                        <a href="indomaret" class="btn-primary">Selengkapnya</a>

                        <div class="periode">
                            <p>
                                <strong>Periode Promo</strong><br>
                                1 Desember 2014 - 15 Januari 2015
                            </p>
                        </div>
                    </div>
                    <div class="prize right">
                        <img src="images/indomaret-prize.png">
                    </div>
                </div>
                <div class="mobile-content">
                    <div class="text-center">
                        <img src="images/mobile/indomaret-prize.png">
                        <p class="description">Menangkan 20 paket liburan ke Raja Ampat untuk kamu dan 2 orang teman atau keluargamu!</p>
                        <a href="indomaret" class="btn-primary">Selengkapnya</a>
                        <div class="periode">
                            <p>
                                <strong>Periode Promo</strong><br>
                                1 Desember 2014 - 15 Januari 2015
                            </p>
                         </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="page-5" class="page front">
            <div id="sevel" class="row clearfix">
                <div class="desktop-content">
                    <div class="headline left">
                        <img src="images/sevel-headline.png">
                        <div class="details">
                            <p>Mainkan berbagai permainan seru khas Indonesia di outlet 7-Eleven terpilih! Tantang dirimu di Pojok Main Temukan Indonesiamu!</p>
                            <a href="seven-eleven" class="btn-primary">Selengkapnya</a>
                        </div>
                    </div>
                    <div class="prize right">
                        <img src="images/sevel-prize.png">
                        <div class="periode">
                            <p>
                                <strong>Periode Promo</strong><br>
                                1 - 27 Desember 2014
                            </p>
                        </div>
                    </div>
                </div>

                <div class="mobile-content">
                    <div class="text-center">
                        <img src="images/mobile/sevel-prize.png">
                        <p class="description">Mainkan berbagai permainan seru khas Indonesia di outlet 7-Eleven terpilih! Tantang dirimu di Pojok Main Temukan Indonesiamu!</p>
                        <a href="seven-eleven" class="btn-primary">Selengkapnya</a>
                        <div class="periode">
                            <p>
                                <strong>Periode Promo</strong><br>
                                1 - 27 Desember 2014
                            </p>
                         </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="page-6" class="page front">
            <div id="circle-k" class="row">
                <div class="desktop-content">
                    <ul class="list">
                        <li>
                            <div class="periode">
                            <p>
                                <strong>Periode Promo</strong><br>
                                7 Desember 2014 - 31 Desember 2014
                            </p>
                            </div>
                        </li>
                        <li>
                            <img src="images/ck-prize.png">
                        </li>
                        <li>
                          <div class="box">
                            <p>Kunjungi outlet Circle K terpilih di Yogjakarta dan foto kamu bersama teman-temanmu di photo booth Temukan Indonesiamu!</p>
                            <a href="circle-k" class="btn-primary">Selengkapnya</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="mobile-content">
                    <div class="text-center">
                        <img src="images/mobile/ck-prize.png">
                        <p class="description">Kunjungi outlet Circle K terpilih di Yogjakarta dan foto kamu bersama teman-temanmu di photo booth Temukan Indonesiamu!</p>
                        <a href="circle-k" class="btn-primary">Selengkapnya</a>
                        <div class="periode">
                            <p>
                                <strong>Periode Promo</strong><br>
                                7 Desember 2014 - 31 Desember 2014
                            </p>
                         </div>
                    </div>
                </div>
            </div>
        </div>
<?php include('footer.php') ?>