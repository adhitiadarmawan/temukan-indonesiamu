<?php $siteURL = "http://localhost:81/temukanindonesiamu";
      $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>

<!DOCTYPE html >
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $title;?></title>
        <meta name="description" content="Kenali Indonesiamu melalui kebersamaan dalam permainan tradisional!">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="images/favicon.png" />

        <meta property="og:title" content="<?php echo $title;?>" />
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="Temukan Indonesiamu"/>
        <meta property="og:url" content="<?php echo $actual_link;?>" />
        <meta property="og:image" content="images/og-image.jpg" />

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/style.min.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
    </head>
    <body class="<?php echo $bodyClass;?>">
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-57216053-1', 'auto');
      ga('send', 'pageview');

    </script>
    <img src="http://serve.vdopia.com/adserver/tracker.php?m=vi&ci=42019&ai=426004&chid=3020&ou=rd&rand=1417086914" width="1" height="1" style="display:none;">
    <div id="dvLoading">
        <div class="row">
          <img src="images/loader.gif" id="loader">
        </div>
    </div>
    <div id="logo"><a href="<?php echo $siteURL; ?>"><img src="images/logo.png" alt="logo AQUA"></a></div>
    <div class="social">
        <a href="http://facebook.com/sehatAQUA" target="_blank"><img src="images/facebook.png" alt="facebook AQUA"></a>
        <a href="http://twitter.com/sehatAQUA" target="_blank"><img src="images/twitter.png" alt="twitter AQUA"></a>
        <a href="http://instagram.com/sehatAQUA" target="_blank"><img src="images/instagram.png" alt="instagram AQUA"></a>
    </div>