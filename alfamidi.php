<?php
 $title = "Alfamidi | AQUA -  Temukan Indonesiamu";
 $bodyClass = "single alfamidi";
 include('header.php') ?>
    <header id="header" class="">
        <div class="logo w-1000"><img src="images/single/alfamidi-logo.png" alt="Temukan Indonesiamu di AlfaMidi/AlfaExpress/Lawson"></div>
        <div class="mobile-content">
            <img src="images/single/alfamidi-prize-mobile.png" class="prize">
        </div>
        <div id="prize-image">
            <img src="images/single/alfamidi-prize.png" style="width:1000px;position:relative;top:75px;">
            <img src="images/single/alfamart-prize-2.png" class="gadget">
        </div>
    </header>     
    <div id="post" class="row clearfix">
        <article class="half">
            <h3>Caranya Mudah</h3>
            <ul class="list-with-images">
                <li>
                    <img src="images/single/alfamart-step1.png">
                    <div class="details">
                        <p>Beli <strong>3 botol AQUA</strong> kemasan 600 ml di AlfaMidi/AlfaExpress/Lawson.</p>
                    </div>
                </li>
                <li>
                    <img src="images/single/alfamart-step2.png">
                    <div class="details">
                        <p>Kirim kode unik yang ada di ekor struk melalui SMS ke nomor <strong>0812 937 000 60</strong>.</p>
                        <div class="box-blue">
                            <h4>Format SMS</h4>
                            <p>AlfaMart#NomorKTP#Nama #NomorTransaksi#KodeUnik</p>
                            <small>(Tarif SMS berlaku normal)</small>
                        </div>
                    </div>
                </li>
            </ul>

            <h3>Ketentuan</h3>
            <ol>
                <li>Setiap pemenang dapat membawa 2 orang teman atau keluarga</li>
                <li>Promo berlaku di seluruh minimarket modern di Pulau Jawa yang berpartisipasi dalam promo ini, termasuk Alfamart, Alfamidi, Alfa Express dan Lawson</li>
                <li>Pemenang akan diumumkan 3 minggu setelah periode promo berakhir</li>
            </ol>
        </article>
        <aside id="sidebar">
            <div class="box">
                <h3>Periode Promo</h3>
                <span>1 Desember 2014 – 15 Januari 2015</span>
            </div>
            <div class="share-button">
                <h3>Share This Page</h3>
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $actual_link; ?>" target="_blank"><img src="images/single/fb.png"></a>
                <a href="http://twitter.com/intent/tweet?text=<?php echo $title; ?> <?php echo $actual_link; ?>"><img src="images/single/twitter.png"></a>
               <!--  <a href=""><img src="images/single/gplus.png"></a> -->
            </div>
        </aside>
    </div>

<?php include('footer.php') ?>