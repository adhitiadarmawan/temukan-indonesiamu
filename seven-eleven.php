<?php
 $title = "Seven Eleven | AQUA -  Temukan Indonesiamu";
 $bodyClass = "single sevel";
 include('header.php') ?>
    <header id="header" class="">
        <div class="logo w-1000"><img src="images/single/sevel-logo.png" alt="Temukan Indonesiamu di Seven Eleven"></div>
        <div class="mobile-content">
            <img src="images/single/sevel-prize-mobile.png" class="prize">
        </div>
        <div id="prize-image">
            <img src="images/single/sevel-prize.png" alt="Temukan Hadiah di Seven Eleven lewat Temukan Indonesiamu">
        </div>
    </header>     

    <div id="post" class="row clearfix">
        <article class="half">
            <h3>Caranya Mudah</h3>
            <ul class="list-with-images">
                <li>
                    <img src="images/single/alfamart-step1.png">
                    <div class="details">
                        <p>Beli <strong>3 botol AQUA</strong> kemasan 600 ml di 7-Eleven.</p>
                    </div>
                </li>
                <li>
                    <img src="images/single/sevel-step2.png">
                    <div class="details">
                        <p>Pilih lokasi Pojok Main kamu. Harap perhatikan tanggal pendaftaran &amp; tanggal bermainnya.</p>
                    </div>
                </li>
                <li>
                    <img src="images/single/sevel-step3.png">
                    <div class="details">
                        <p>Upload foto struk + botol AQUA kemasan 600 ml ke Twitter.</p>
                        <div class="box-blue">
                            <h4>Format Tweet:</h4>
                            <p>(Outlet Pojok Main Pilihanmu)<spasi> #TemukanIndonesiamu<spasi>@SehatAQUA<spasi> @7ElevenID</p>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="images/single/sevel-step4.png">
                    <div class="details">
                        <p>Tunggu tweet validasi dari @SehatAQUA H-3 sebelum acara di mulai dan datang ke Pojok Main #TemukanIndonesiamu.</p>
                    </div>
                </li>                
            </ul>

            <h3>Lokasi Pojok Main</h3>
            <ul class="no-list-style">
                <li>
                    <strong>7-Eleven Kalimalang</strong><br>
                    13 Desember 2014<br>
                    Pendaftaran hingga 7 Desember 2014
                </li>
                <li>
                    <strong>7-Eleven Teluk Betung</strong><br>
                    20 Desember 2014<br>
                    Pendaftaran hingga 14 Desember 2014
                </li>
                <li>
                    <strong>7-Eleven Bintaro Sektor 7</strong><br>27 Desember 2014<br>Pendaftaran hingga 21 Desember 2014
                </li>
            </ul>

            <h3>Ketentuan</h3>
            <ol>
                <li>Dua orang pemenang akan dipilih dari setiap lokasi Pojok Main</li>
                <li>Promo berlaku di 7-Eleven</li>
            </ol>
        </article>
        <aside id="sidebar">
        <h3>Jenis Permainan</h3>
            <div id="jenis-permainan">

                <ul>
                    <li class="clearfix">
                        <div class="images">
                            <img src="images/single/gasing.png">
                        </div>
                        <div class="details">
                            <h4>Gasing</h4>
                            <p>Merupakan permainan asli Indonesia, banyak ditemukan di daerah pesisir pantai. Dahulu, permainan gasing digunakan untuk meramal. Perputaran gasing merupakan cerminan kehidupan yang terus berputar walau harus berputar di tempat yang tidak rata.</p>
                        </div>
                    </li>
                    <li class="clearfix">
                        <div class="images">
                            <img src="images/single/egrang.png">
                        </div>
                        <div class="details">
                            <h4>Egrang</h4>
                            <p>Adalah permainan ASLI dari 
                            Indonesia yang sering dijadikan 
                            ajang balap lari alias balap egrang. 
                            Rekor MURI berjalan dengan egrang 
                            terjauh adalah 228 km. Permainan 
                            ini disesuaikan dengan umur pemain. 
                            Semakin muda umurnya, semakin 
                            kecil ukuran egrang dan semakin 
                            mudah aturan mainnya.</p>
                        </div>
                    </li>
                    <li class="clearfix">
                        <div class="images">
                            <img src="images/single/catur-jawa.png">
                        </div>
                        <div class="details">
                            <h4>Catur Jawa</h4>
                            <p>Adalah satu dari banyak 
                            modifikasi permainan catur 
                            yang ada di seluruh dunia. 
                            Strategi terbaik untuk 
                            memenangkan permainan dapat 
                            dianalisa melalui algoritma 
                            genetik. Permainannya, bukan 
                            ‘makan-memakan’, tetapi siapa 
                            yang paling cepat sampai 
                            tujuan dari lawan.</p>
                        </div>
                    </li>
                    <li class="clearfix">
                        <div class="images">
                            <img src="images/single/congklak.png">
                        </div>
                        <div class="details">
                            <h4>Congklak</h4>
                            <p>Adalah permainan dengan 
                            pemilihan strategi yang tepat 
                            untuk mengumpulkan rezeki 
                            paling cepat dan paling banyak 
                            dari lawan. Dahulu, biji congklak 
                            yang digunakan untuk bermain 
                            bukan menggunakan kerang, 
                            tetapi menggunakan biji kopi.</p>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="box">
                <h3>Periode Promo</h3>
                <span>1 - 27 Desember 2014</span>
            </div>
            <div class="share-button">
                <h3>Share This Page</h3>
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $actual_link; ?>" target="_blank"><img src="images/single/fb.png"></a>
                <a href="http://twitter.com/intent/tweet?text=<?php echo $title; ?> <?php echo $actual_link; ?>"><img src="images/single/twitter.png"></a>
               <!--  <a href=""><img src="images/single/gplus.png"></a> -->
            </div>
        </aside>
    </div>

<?php include('footer.php') ?>