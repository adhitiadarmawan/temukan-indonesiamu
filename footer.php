
        <footer id="footer">
            <div class="row clearfix">
                <div id="copyright" class="left">
                    <span>Copyright &copy; 2014 PT Tirta Investama. All rights reserved</span>
                </div>
                <div id="menu" class="right">
                    <a href="http://aqua.com/" target="_blank">AQUA.com</a>
                </div>
            </div>
        </footer>
        <script src="js/plugins.js"></script>
        <script src="js/vendor/parallax.js"></script>
        <script src="js/script.js"></script>


    </body>
</html>
